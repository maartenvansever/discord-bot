import discord
import os
import json
import aiohttp
import asyncio

client = discord.Client()

angry_words = ["fack you"]
sad_words = ["sad"]
warnings = ["let op uw woorden!"]

async def get_random_giphy():
  async with aiohttp.ClientSession() as session:
    async with session.get('https://api.giphy.com/v1/gifs/random?api_key=Mi9wUbceep5Vlk53BA3yGN5SlAKqz5DK&tag=&rating=g') as response:
        res = await response.json()
        return res['data']['url']

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    msg = message.content
    if msg.startswith('$hello'):
        await message.channel.send('Hello, how are you doing today?')
    if msg.startswith('$joke'):
        await message.channel.send('je moeder :rofl:')
    if msg.startswith('$randomgiphy'):
        giphy = await get_random_giphy()
        await message.channel.send(giphy)
    if any(word in msg for word in angry_words):
        await message.channel.send('let op uw woorden!')
    if any(word in msg for word in sad_words):
            await message.channel.send('het komt allemaal goed')
client.run('ODQwNjM1MzEzNDg2NTYxMzQw.YJbEpA.RirLVzMG1Ns0Cn_eXpDnaLo-6a4')